package com.dt.platform.constants.enums.dict;

import com.github.foxnic.api.constant.CodeTextEnum;
import com.github.foxnic.commons.reflect.EnumUtil;



/**
 * @since 2021-09-29 11:26:59
 * @author 金杰 , maillank@qq.com
 * 此文件由工具自动生成，请勿修改。若表结构变动，请使用工具重新生成。
*/

public enum EamSource implements CodeTextEnum {
	
	/**
	 * 采购
	*/
	PURCHASE("purchase" , "采购"),
	
	/**
	 * 自建
	*/
	SELFBUILD("selfbuild" , "自建"),
	
	/**
	 * 捐赠
	*/
	DONATION("donation" , "捐赠"),
	
	/**
	 * 其他
	*/
	OTHER("other" , "其他"),
	;
	
	private String code;
	private String text;
	private EamSource(String code,String text)  {
		this.code=code;
		this.text=text;
	}
	
	public String code() {
		return code;
	}
	
	public String text() {
		return text;
	}
	
	/**
	 * 从字符串转换成当前枚举类型
	*/
	public static EamSource parseByCode(String code) {
		return (EamSource) EnumUtil.parseByCode(EamSource.values(),code);
	}
}